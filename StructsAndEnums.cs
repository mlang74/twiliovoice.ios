using System;
using ObjCRuntime;

namespace TwilioVoice.iOS
{
    [Native]
    public enum TCConnectionState : ulong
    {
        [DefaultEnumValue]
        Pending = 0,
        Connecting,
        Connected,
        Disconnected
    }

    [Native]
    public enum TCDeviceState : ulong
    {
        [DefaultEnumValue]
        Offline = 0,
        Ready,
        Busy
    }

    [Native]
    public enum TCLogLevel : ulong
    {
        [DefaultEnumValue]
        Off = 0,
        Error,
        Warn,
        Info,
        Debug,
        Verbose
    }
}
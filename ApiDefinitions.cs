using System;
using Foundation;
using ObjCRuntime;

namespace TwilioVoice.iOS
{
    // @protocol TCConnectionDelegate <NSObject>
    [Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface TCConnectionDelegate
    {
        // @required -(void)connection:(TCConnection * _Nonnull)connection didFailWithError:(NSError * _Nullable)error;
        [Abstract]
        [Export("connection:didFailWithError:")]
        void Connection(TCConnection connection, [NullAllowed] NSError error);

        // @optional -(void)connectionDidStartConnecting:(TCConnection * _Nonnull)connection;
        [Export("connectionDidStartConnecting:")]
        void ConnectionDidStartConnecting(TCConnection connection);

        // @optional -(void)connectionDidConnect:(TCConnection * _Nonnull)connection;
        [Export("connectionDidConnect:")]
        void ConnectionDidConnect(TCConnection connection);

        // @optional -(void)connectionDidDisconnect:(TCConnection * _Nonnull)connection;
        [Export("connectionDidDisconnect:")]
        void ConnectionDidDisconnect(TCConnection connection);
    }

    [Static]
    partial interface Constants
    {
        // extern NSString *const _Nonnull TCConnectionIncomingParameterFromKey;
        [Field("TCConnectionIncomingParameterFromKey", "__Internal")]
        NSString TCConnectionIncomingParameterFromKey { get; }

        // extern NSString *const _Nonnull TCConnectionIncomingParameterToKey;
        [Field("TCConnectionIncomingParameterToKey", "__Internal")]
        NSString TCConnectionIncomingParameterToKey { get; }

        // extern NSString *const _Nonnull TCConnectionIncomingParameterAccountSIDKey;
        [Field("TCConnectionIncomingParameterAccountSIDKey", "__Internal")]
        NSString TCConnectionIncomingParameterAccountSIDKey { get; }

        // extern NSString *const _Nonnull TCConnectionIncomingParameterAPIVersionKey;
        [Field("TCConnectionIncomingParameterAPIVersionKey", "__Internal")]
        NSString TCConnectionIncomingParameterAPIVersionKey { get; }

        // extern NSString *const _Nonnull TCConnectionIncomingParameterCallSIDKey __attribute__((deprecated("")));
        [Field("TCConnectionIncomingParameterCallSIDKey", "__Internal")]
        NSString TCConnectionIncomingParameterCallSIDKey { get; }

        // extern NSString *const _Nonnull TCConnectionParameterCallSIDKey;
        [Field("TCConnectionParameterCallSIDKey", "__Internal")]
        NSString TCConnectionParameterCallSIDKey { get; }
    }

    // @interface TCConnection : NSObject
    [BaseType(typeof(NSObject))]
    interface TCConnection
    {
        // @property (readonly, assign, nonatomic) TCConnectionState state;
        [Export("state", ArgumentSemantic.Assign)]
        TCConnectionState State { get; }

        // @property (readonly, getter = isIncoming, assign, nonatomic) BOOL incoming;
        [Export("incoming")]
        bool Incoming { [Bind("isIncoming")] get; }

        // @property (readonly, nonatomic, weak) NSDictionary<NSString *,NSString *> * _Nullable parameters;
        [NullAllowed, Export("parameters", ArgumentSemantic.Weak)]
        NSDictionary<NSString, NSString> Parameters { get; }

        [Wrap("WeakDelegate")]
        [NullAllowed]
        TCConnectionDelegate Delegate { get; set; }

        // @property (nonatomic, weak) id<TCConnectionDelegate> _Nullable delegate;
        [NullAllowed, Export("delegate", ArgumentSemantic.Weak)]
        NSObject WeakDelegate { get; set; }

        // @property (getter = isMuted, assign, nonatomic) BOOL muted;
        [Export("muted")]
        bool Muted { [Bind("isMuted")] get; set; }

        // -(void)accept;
        [Export("accept")]
        void Accept();

        // -(void)ignore;
        [Export("ignore")]
        void Ignore();

        // -(void)reject;
        [Export("reject")]
        void Reject();

        // -(void)disconnect;
        [Export("disconnect")]
        void Disconnect();

        // -(void)sendDigits:(NSString * _Nonnull)digits;
        [Export("sendDigits:")]
        void SendDigits(string digits);
    }

    // @protocol TCDeviceDelegate <NSObject>
    [Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface TCDeviceDelegate
    {
        // @required -(void)device:(TCDevice * _Nonnull)device didStopListeningForIncomingConnections:(NSError * _Nullable)error;
        [Abstract]
        [Export("device:didStopListeningForIncomingConnections:")]
        void Device(TCDevice device, [NullAllowed] NSError error);

        // @optional -(void)deviceDidStartListeningForIncomingConnections:(TCDevice * _Nonnull)device;
        [Export("deviceDidStartListeningForIncomingConnections:")]
        void DeviceDidStartListeningForIncomingConnections(TCDevice device);

        // @optional -(void)device:(TCDevice * _Nonnull)device didReceiveIncomingConnection:(TCConnection * _Nonnull)connection;
        [Export("device:didReceiveIncomingConnection:")]
        void Device(TCDevice device, TCConnection connection);

        // @optional -(void)device:(TCDevice * _Nonnull)device didReceivePresenceUpdate:(TCPresenceEvent * _Nonnull)presenceEvent;
        [Export("device:didReceivePresenceUpdate:")]
        void Device(TCDevice device, TCPresenceEvent presenceEvent);
    }

    //can't apply multiple times... [Static]
    partial interface Constants
    {
        // extern NSString *const _Nonnull TCDeviceCapabilityIncomingKey;
        [Field("TCDeviceCapabilityIncomingKey", "__Internal")]
        NSString TCDeviceCapabilityIncomingKey { get; }

        // extern NSString *const _Nonnull TCDeviceCapabilityOutgoingKey;
        [Field("TCDeviceCapabilityOutgoingKey", "__Internal")]
        NSString TCDeviceCapabilityOutgoingKey { get; }

        // extern NSString *const _Nonnull TCDeviceCapabilityExpirationKey;
        [Field("TCDeviceCapabilityExpirationKey", "__Internal")]
        NSString TCDeviceCapabilityExpirationKey { get; }

        // extern NSString *const _Nonnull TCDeviceCapabilityAccountSIDKey;
        [Field("TCDeviceCapabilityAccountSIDKey", "__Internal")]
        NSString TCDeviceCapabilityAccountSIDKey { get; }

        // extern NSString *const _Nonnull TCDeviceCapabilityApplicationSIDKey;
        [Field("TCDeviceCapabilityApplicationSIDKey", "__Internal")]
        NSString TCDeviceCapabilityApplicationSIDKey { get; }

        // extern NSString *const _Nonnull TCDeviceCapabilityApplicationParametersKey;
        [Field("TCDeviceCapabilityApplicationParametersKey", "__Internal")]
        NSString TCDeviceCapabilityApplicationParametersKey { get; }

        // extern NSString *const _Nonnull TCDeviceCapabilityClientNameKey;
        [Field("TCDeviceCapabilityClientNameKey", "__Internal")]
        NSString TCDeviceCapabilityClientNameKey { get; }
    }

    // @interface TCDevice : NSObject
    [BaseType(typeof(NSObject))]
    interface TCDevice
    {
        // @property (readonly, assign, nonatomic) TCDeviceState state;
        [Export("state", ArgumentSemantic.Assign)]
        TCDeviceState State { get; }

        // @property (readonly, nonatomic, weak) NSDictionary<NSString *,id> * _Nullable capabilities;
        [NullAllowed, Export("capabilities", ArgumentSemantic.Weak)]
        NSDictionary<NSString, NSObject> Capabilities { get; }

        [Wrap("WeakDelegate")]
        [NullAllowed]
        TCDeviceDelegate Delegate { get; set; }

        // @property (nonatomic, weak) id<TCDeviceDelegate> _Nullable delegate;
        [NullAllowed, Export("delegate", ArgumentSemantic.Weak)]
        NSObject WeakDelegate { get; set; }

        // @property (assign, nonatomic) BOOL incomingSoundEnabled;
        [Export("incomingSoundEnabled")]
        bool IncomingSoundEnabled { get; set; }

        // @property (assign, nonatomic) BOOL outgoingSoundEnabled;
        [Export("outgoingSoundEnabled")]
        bool OutgoingSoundEnabled { get; set; }

        // @property (assign, nonatomic) BOOL disconnectSoundEnabled;
        [Export("disconnectSoundEnabled")]
        bool DisconnectSoundEnabled { get; set; }

        // -(id _Nullable)initWithCapabilityToken:(NSString * _Nonnull)capabilityToken delegate:(id<TCDeviceDelegate> _Nullable)delegate;
        [Export("initWithCapabilityToken:delegate:")]
        IntPtr Constructor(string capabilityToken, [NullAllowed] TCDeviceDelegate @delegate);

        // -(void)listen;
        [Export("listen")]
        void Listen();

        // -(void)unlisten;
        [Export("unlisten")]
        void Unlisten();

        // -(void)updateCapabilityToken:(NSString * _Nonnull)capabilityToken;
        [Export("updateCapabilityToken:")]
        void UpdateCapabilityToken(string capabilityToken);

        // -(TCConnection * _Nullable)connect:(NSDictionary<NSString *,NSString *> * _Nullable)parameters delegate:(id<TCConnectionDelegate> _Nullable)delegate;
        [Export("connect:delegate:")]
        [return: NullAllowed]
        TCConnection Connect([NullAllowed] NSDictionary<NSString, NSString> parameters, [NullAllowed] TCConnectionDelegate @delegate);

        // -(void)disconnectAll;
        [Export("disconnectAll")]
        void DisconnectAll();
    }

    // @interface TCPresenceEvent : NSObject
    [BaseType(typeof(NSObject))]
    interface TCPresenceEvent
    {
        // @property (readonly, nonatomic, strong) NSString * _Nonnull name;
        [Export("name", ArgumentSemantic.Strong)]
        string Name { get; }

        // @property (readonly, getter = isAvailable, assign, nonatomic) BOOL available;
        [Export("available")]
        bool Available { [Bind("isAvailable")] get; }
    }

    // @interface TwilioClient : NSObject
    [BaseType(typeof(NSObject))]
    interface TwilioClient
    {
        // +(id _Nonnull)sharedInstance;
        [Static]
        [Export("sharedInstance")]
        NSObject SharedInstance { get; }

        // @property (readonly, nonatomic, strong) NSString * _Nonnull version;
        // +(NSString * _Nonnull)version;
        [Static]
        [Export("version", ArgumentSemantic.Strong)]
        string Version { get; }

        // -(void)setLogLevel:(TCLogLevel)level;
        [Export("setLogLevel:")]
        void SetLogLevel(TCLogLevel level);

        // -(void)setMetricsEnabled:(BOOL)enabled;
        [Export("setMetricsEnabled:")]
        void SetMetricsEnabled(bool enabled);
    }
}